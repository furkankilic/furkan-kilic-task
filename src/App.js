import React, { Component } from 'react';
import axios from 'axios';
import { Button, FormControl, FormGroup, ControlLabel} from 'react-bootstrap';
import 'react-select/dist/react-select.css';
import Paragraphs from './components/Paragraphs';
import Clicked from './components/Clicked';


class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedParagraph: '',
      pick: 'asdf'
    }
}

  componentDidMount() {
    axios.get(`https://qa-sum-dev.herokuapp.com/text`) 
      .then(res => {
        const text = res.data.result.metin;
        this.setState({ 
          selectedParagraph: text
        });
      })
  }

  divideToParas() {
    const paras = this.state.selectedParagraph.split(/\n\s*\n/g);
    let options = [];
    for(let i in paras) {
      options.push({value: paras[i], label: paras[i]});
    }
    return options;
  }


  render() {
    console.log('pick', this.state.pick)
    const options = this.divideToParas();
    return(
      <div>
        <Paragraphs
          name="form-field-name"
          className="left-half"
          value={this.state.selectedParagraph}
          onClick={this.handleClick}
          options={options}
        />  
        <div 
          className="right-half" 
        >
          <Clicked 
            pick={this.state.pick}
          />
          <form>
            <FormGroup controlId="formControlsTextarea">
              <ControlLabel>Soru</ControlLabel>
              <FormControl componentClass="textarea" placeholder="Sorunuzu giriniz" required/>
            </FormGroup>
            <FormGroup controlId="formControlsTextarea">
              <ControlLabel>Cevap</ControlLabel>
              <FormControl componentClass="textarea" placeholder="Cevabınızı giriniz" required/>
            </FormGroup>
          </form>  
          <Button bsStyle="default" bsSize="large">Yolla</Button>
        </div> 
      </div>
    )
  }
}

export default App;
