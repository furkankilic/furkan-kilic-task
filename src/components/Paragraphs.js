import React from 'react';

export default ({options, click}) => {

    const listItem = options.map(articles => {
        return (
            <li 
                key={articles.value}
                className="paragraph"
                onClick={() => click(articles.value)}
             >
                {articles.value}
            </li> 
        )
    })
    console.log('listItem', listItem)
    return (
    <section className="container">
        <div className="left-half">
            <article>
                <ul>
                    {listItem}
                </ul>
            </article>
        </div>
        <div className="right-half"></div>
    </section>
    );
}